/* Description
	Proposed IEEE standard library enhancements.
	
	@designer(s):
	-	Daniel C.K. Kho	|	daniel.kho (at) logik [dot] haus
	
	@license: LogikHaus License Agreement.
	
	Copyright© 2012-2019 Tauhop Solutions. All Rights Reserved.
	Copyright© 2019		 LogikHaus Sdn. Bhd. All Rights Reserved.
	
	Your use of LogikHaus' Intellectual Property, including, without limitation, 
	design tools, hardware design source files, schematics, software source code, 
	scripts, and other software and tools, and its partners' Intellectual Property, 
	and any output files of the foregoing, and any associated documentation or 
	information, are expressly subject to the terms and conditions of the LogikHaus 
	License Agreement, or other applicable license agreement. Please refer to the 
	applicable agreement for further details.
	
	Revision History: @see Git log for full list of changes.
	
	This notice and disclaimer must be retained as part of this text at all times.
*/

package standard is
	/* Reference definitions from VHDL-2008 LRM. */
	--type	boolean					is (false, true);
	--type	bit						is ('0', '1');
	----type	character				is (...);
	--type	integer					is range -2147483648	to 2147483647;
	--
	--type	real					is range -1.0e308		to 1.0e308;
	--type	time					is range -2147483647	to 2147483647
	--	units
	--		fs;
	--		ps	= 1000 fs;
	--		ns	= 1000 ps;
	--		us	= 1000 ns;
	--		ms	= 1000 us;
	--		sec	= 1000 ms;
	--		min	= 60 sec;
	--		hr	= 60 min;
	--	end units;
	
	
	type	unresolved_boolean			is (false, true, undef);
	type	unresolved_bit				is ('0', '1', undef);
	--type	unresolved_character		is (...);
	--type	unresolved_integer			is range -2147483648	to 2147483647;
	type	unresolved_real				is range -1.0e308		to 1.0e308;
	type	unresolved_time				is range -2147483647	to 2147483647
		units
			fs;
			ps	= 1000 fs;
			ns	= 1000 ps;
			us	= 1000 ns;
			ms	= 1000 us;
			sec	= 1000 ms;
			min	= 60 sec;
			hr	= 60 min;
		end units;
	
	--alias	unresolved_boolean			is boolean;
	--alias	unresolved_bit				is bit;
	alias	unresolved_character		is character;			/* Use NUL character as metavalue. */
	--alias	unresolved_integer			is integer;
	--alias	unresolved_real				is real;
	--alias	unresolved_time				is time;
	
	alias	u_boolean					is unresolved_boolean;
	alias	u_bit						is unresolved_bit;
	alias	u_character					is unresolved_character;
	alias	u_integer					is unresolved_integer;
	alias	u_real						is unresolved_real;
	alias	u_time						is unresolved_time;
	
	type	associative_array			is record
	end record associative_array;
	
	type	universal_integer			is record
		val:	integer range -2147483648	to 2147483647;
		undefs:	u_bit;
	end record unresolved_integer;
	
	
	type	unresolved_boolean_vector	is array(integer	range <>) of u_boolean;
	type	unresolved_bit_vector		is array(integer	range <>) of u_bit;
	type	unresolved_string			is array(positive	range <>) of u_character; 
	type	unresolved_integer_vector	is array(integer	range <>) of u_integer;
	type	unresolved_real_vector		is array(integer	range <>) of u_real;
	type	unresolved_time_vector		is array(integer	range <>) of u_time;
	
	alias	u_boolean_vector			is unresolved_boolean_vector;
	alias	u_bit_vector				is unresolved_bit_vector;
	alias	u_string					is unresolved_string;
	alias	u_integer_vector			is unresolved_integer_vector;
	alias	u_real_vector				is unresolved_real_vector;
	alias	u_time_vector				is unresolved_time_vector;
	
	subtype	resolved_boolean			is resolved u_boolean;
	subtype resolved_bit				is resolved u_bit;
	subtype resolved_character			is resolved u_character;
	subtype resolved_integer			is resolved u_integer;
	subtype resolved_real				is resolved u_real;
	subtype resolved_time				is resolved u_time;
	
	subtype resolved_boolean_vector		is (resolved) u_boolean_vector;
	subtype resolved_bit_vector			is (resolved) u_bit_vector;
	subtype resolved_string				is (resolved) u_string;
	subtype resolved_integer_vector		is (resolved) u_integer_vector;
	subtype resolved_real_vector		is (resolved) u_real_vector;
	subtype resolved_time_vector		is (resolved) u_time_vector;
	
	alias	r_boolean					is resolved_boolean;
	alias	r_bit						is resolved_bit;
	alias	r_character					is resolved_character;
	alias	r_integer					is resolved_integer;
	alias	r_real						is resolved_real;
	alias	r_time						is resolved_time;
	
	alias	r_boolean_vector			is resolved_boolean_vector;
	alias	r_bit_vector				is resolved_bit_vector;
	alias	r_string					is resolved_string;
	alias	r_integer_vector			is resolved_integer_vector;
	alias	r_real_vector				is resolved_real_vector;
	alias	r_time_vector				is resolved_time_vector;
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* IEEE Proposed:
		Unresolved types.
	*/
	type unresolved_boolean		is ('X',false,true);
	type unresolved_bit			is ('X','0','1');
	type unresolved_character	is (
		NUL, SOH, STX, ETX, EOT, ENQ, ACK, BEL, 
		BS,  HT,  LF,  VT,  FF,  CR,  SO,  SI, 
		DLE, DC1, DC2, DC3, DC4, NAK, SYN, ETB, 
		CAN, EM,  SUB, ESC, FSP, GSP, RSP, USP, 

		' ', '!', '"', '#', '$', '%', '&', ''', 
		'(', ')', '*', '+', ',', '-', '.', '/', 
		'0', '1', '2', '3', '4', '5', '6', '7', 
		'8', '9', ':', ';', '<', '=', '>', '?', 

		'@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 
		'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 
		'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 
		'X', 'Y', 'Z', '[', '\', ']', '^', '_', 

		'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 
		'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 
		'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
		'x', 'y', 'z', '{', '|', '}', '~', DEL,

		C128, C129, C130, C131, C132, C133, C134, C135,
		C136, C137, C138, C139, C140, C141, C142, C143,
		C144, C145, C146, C147, C148, C149, C150, C151,
		C152, C153, C154, C155, C156, C157, C158, C159,

		-- the character code for 160 is there (NBSP), 
		-- but prints as no char 

		' ', '¡', '¢', '£', '¤', '¥', '¦', '§',
		'¨', '©', 'ª', '«', '¬', '­', '®', '¯',
		'°', '±', '²', '³', '´', 'µ', '¶', '·',
		'¸', '¹', 'º', '»', '¼', '½', '¾', '¿',

		'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç',
		'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï',
		'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×',
		'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß',

		'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç',
		'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï',
		'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷',
		'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'þ', 'ÿ' 
	); 
	
	/* TODO Add undefined values. */
	--type unresolved_integer is (range -2147483648 to 2147483647, undefined);
	type unresolved_integer	is range -2147483648	to 2147483647;
	
	
	type unresolved_real	is range -1.0e308		to 1.0e308;
	type unresolved_time	is range -2147483647	to 2147483647
		units
			fs;
			ps	= 1000 fs;
			ns	= 1000 ps;
			us	= 1000 ns;
			ms	= 1000 us;
			sec	= 1000 ms;
			min	= 60 sec;
			hr	= 60 min;
		end units;
	
	alias u_boolean		is unresolved_boolean;
	alias u_bit			is unresolved_bit;
	alias u_character	is unresolved_character;
	alias u_integer		is unresolved_integer;
	alias u_real		is unresolved_real;
	alias u_time		is unresolved_time;
	
	subtype	natural		is u_integer range 0 to u_integer'high;
	subtype	positive	is u_integer range 1 to u_integer'high;
	
	type u_boolean_vector	is array(integer	range <>) of u_boolean;
	type u_bit_vector		is array(integer	range <>) of u_bit;
	type u_string			is array(positive	range <>) of u_character; 
	type u_integer_vector	is array(integer	range <>) of u_integer;
	type u_real_vector		is array(integer	range <>) of u_real;
	type u_time_vector		is array(integer	range <>) of u_time;
	
	/* IEEE Proposed:
		Resolution functions that resolves multiple drivers to undefined 
		values.
		
		TODO allow flexibility to allow or disallow multiple-driver 
		resolution. If multiple drivers are allowed, proper determination 
		of muti-state values are needed (similar to std_logic resolution).
	*/
	function resolved(s: u_boolean_vector)	return u_boolean;
	function resolved(s: u_bit_vector)		return u_bit;
	function resolved(s: u_string)			return u_character;
	function resolved(s: u_integer_vector)	return u_integer;
	function resolved(s: u_real_vector)		return u_real;
	function resolved(s: u_time_vector)		return u_time;
	
	/* TODO FIXME */
	subtype boolean			is resolved u_boolean;
	subtype bit				is resolved u_bit;
	subtype character		is resolved u_character;
	subtype integer			is resolved u_integer;
	subtype real			is resolved u_real;
	subtype time			is resolved u_time;
	
	subtype boolean_vector	is (resolved) u_boolean_vector;
	subtype bit_vector		is (resolved) u_bit_vector;
	subtype string			is (resolved) u_string;
	subtype integer_vector	is (resolved) u_integer_vector;
	subtype real_vector		is (resolved) u_real_vector;
	subtype time_vector		is (resolved) u_time_vector;
	
	
	type byte				is array(7 downto 0)		of bit;
	type byte_vector		is array(natural range <>)	of byte;
end package standard;

package body standard is
	function "??" (s : u_boolean) return boolean is begin
		return s = true;
	end function "??";
	
	function resolved(s:u_boolean_vector) return u_boolean is
		variable result:u_boolean:='X';
	begin
		for i in s'range loop
			if i=s'low then result:='X'; end if;
			
			if is_valid(s(i)) then
				if is_valid(result) then
					/* Return undefined value when there are multiple drivers. */
					report "[" & time'image(now) & " standard.vhdl] Multiple boolean drivers detected, returning 'X'." severity error;
					return 'X';
				else result:=s(i);
				end if;
			end if;
		end loop;
		return result;
	end function resolved;
	
	
	function resolved(s:u_integer_vector) return u_integer is
		variable result:u_integer:=u_integer'low;
	begin
		for i in s'range loop
			if i=s'low then result:=u_integer'low; end if;
			
			if is_valid(s(i)) then
				if is_valid(result) then
					/* Return undefined value when there are multiple drivers. */
					report "[" & time'image(now) & " standard.vhdl] Multiple integer drivers detected, returning undefined." severity error;
					return u_integer'low;
				else result:=s(i);
				end if;
			end if;
		end loop;
		return result;
	end function resolved;
	
	function resolved(s:u_real_vector) return u_real is
	begin
		return 0.0;
	end function resolved;
	
	function resolved(s:u_time_vector) return u_time is
	begin
		return 0 ps;
	end function resolved;
end package body standard;
